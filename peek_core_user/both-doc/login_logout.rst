=====================
User Login and Logout
=====================

.. image:: user_button.png
    :align: center

.. toctree::
    :maxdepth: 2
    :caption: Contents:

Login
-----
Navigate to the login screen, then:

#. Enter your user name into the first field
#. Enter your password into the second field
#. Click **Login**

.. image:: login.png
    :align: center

.. note:: You may be redirected to register your device if it is not registered.

Logout
------

Click on the **Login / Logout** button

.. image:: user_button.png
    :align: center

----

#. Click the **Logout** button

.. image:: login_logout.png
    :align: center